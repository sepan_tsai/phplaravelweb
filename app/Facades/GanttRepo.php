<?php

namespace App\Facades;

use App\Contracts\Repositories\GanttRepoContract;
use Illuminate\Support\Facades\Facade;

class GanttRepo extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return GanttRepoContract::class;
    }
}
