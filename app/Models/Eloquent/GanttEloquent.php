<?php

namespace App\Models\Eloquent;

use App\Contracts\Entities\Gantt;
use Carbon\Carbon;
use Eloquent;

/**
 * @property int    $id
 * @property string $event
 * @property Carbon $start_at
 * @property Carbon $end_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class GanttEloquent extends Eloquent implements Gantt
{
    protected $table = 'schedule_gantt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'event', 'start_at', 'end_at', 'created_at', 'updated_at',
    ];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @param string $event
     */
    public function setEvent(string $event): void
    {
        $this->event = $event;
    }

    /**
     * @return Carbon
     */
    public function getStartAt(): Carbon
    {
        return $this->start_at;
    }

    /**
     * @param Carbon $start_at
     */
    public function setStartAt(Carbon $start_at): void
    {
        $this->start_at = $start_at;
    }

    /**
     * @return Carbon
     */
    public function getEndAt(): Carbon
    {
        return $this->end_at;
    }

    /**
     * @param Carbon $end_at
     */
    public function setEndAt(Carbon $end_at): void
    {
        $this->end_at = $end_at;
    }
}
