<?php

namespace App\Services;

use App\Contracts\Entities\Gantt;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use JetBrains\PhpStorm\Pure;
use Log;
use Spatie\Permission\Contracts\Role;
use Illuminate\Http\Request;
use GanttRepo;
use RoleRepo;

class GanttService
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    #[Pure] public function __construct()
    {
    }

    /**
     * @param int $id
     *
     * @return Gantt
     */
    public function findById(int $id): Gantt
    {
        return GanttRepo::findById($id);
    }

    /**
     * @return Collection|Gantt[]|null
     */
    public function getAllGanttEvents(): ?Collection
    {
        return GanttRepo::all();
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function updateGanttEvents(Request $request): bool
    {
        $ganttId = $request->get('id');
        try {
            $group = GanttRepo::make();
            $group->setEvent($request->get('event'));
            $group->setStartAt(Carbon::parse($request->get('start_at')));
            $group->setEndAt(Carbon::parse($request->get('end_at')));
            Log::info(json_encode($group->toArray()));
            GanttRepo::updateOrCreate(['id' => $ganttId], $group->toArray());

            return true;
        } catch (Exception $e) {
            Log::info(__CLASS__ . ' - ' . __FUNCTION__ . ' - ' . __LINE__);
            Log::error($e->getMessage());
            return false;
        }
    }

    /**
     * @param int $eventId
     *
     * @return bool
     */
    public function deleteGanttEvents(int $eventId): bool
    {
        try {
            GanttRepo::delete($eventId);
            return true;
        } catch (Exception $e) {
            Log::info(__CLASS__ . ' - ' . __FUNCTION__ . ' - ' . __LINE__);
            Log::error($e->getMessage());
            return false;
        }
    }

    /**
     * @return Collection|Role
     */
    public function getRoles(): Collection|Role
    {
        return RoleRepo::all();
    }
}
