<?php

namespace App\Http\Controllers;

use App\Services\GanttService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\Eloquent\Collection;
use DB;
use Log;
use Exception;
use Throwable;

class GanttController extends Controller
{
    private GanttService $ganttService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->ganttService = new GanttService();
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('home');
    }

    /**
     * @return Collection|null
     */
    public function getAllGanttEvents(): ?Collection
    {
        return $this->ganttService->getAllGanttEvents();
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateGanttEvents(Request $request): JsonResponse
    {
        Log::info(__CLASS__ . ' - ' .  __FUNCTION__ . ' - ' . __LINE__);
        Log::info($request);
        $result = $this->ganttService->updateGanttEvents($request);

        if ($result) {
            $statusCode = 201;
        } else {
            $statusCode = 400;
        }
        return response()->json(['result' => $result], $statusCode);
    }

    /**
     * @param int $eventId
     *
     * @return JsonResponse
     * @throws Throwable
     */
    public function deleteGanttEvents(int $eventId): JsonResponse
    {
        DB::beginTransaction();
        try {
            $result = $this->ganttService->deleteGanttEvents($eventId);
            DB::commit();
        } catch (Exception $e) {
            Log::info(__CLASS__ . ' - ' .  __FUNCTION__ . ' - ' . __LINE__);
            Log::error($e->getMessage());
            DB::rollBack();
            throw $e;
        }

        if ($result) {
            $statusCode = 204;
        } else {
            $statusCode = 400;
        }
        return response()->json(['result' => $result], $statusCode);
    }
}
