<?php

namespace App\Repositories;

use App\Contracts\Entities\Gantt;
use App\Contracts\Repositories\GanttRepoContract;
use App\Models\Eloquent\GanttEloquent;
use Illuminate\Database\Eloquent\Collection;

class GanttRepo extends BaseRepo implements GanttRepoContract
{
    public function make(): Gantt
    {
        return new GanttEloquent();
    }

    /**
     * @inheritDoc
     */
    public function model(): string
    {
        return GanttEloquent::class;
    }

    /**
     * @inheritDoc
     */
    public function findById($id): ?Gantt
    {
        return GanttEloquent::where('id', $id)->first();
    }
}
