<?php

namespace App\Contracts\Entities;

use Carbon\Carbon;

interface Gantt extends BaseEntity
{

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param int $id
     */
    public function setId(int $id): void;

    /**
     * @return string
     */
    public function getEvent(): string;

    /**
     * @param string $event
     */
    public function setEvent(string $event): void;
    /**
     * @return Carbon
     */
    public function getStartAt(): Carbon;

    /**
     * @param Carbon $start_at
     */
    public function setStartAt(Carbon $start_at): void;

    /**
     * @return Carbon
     */
    public function getEndAt(): Carbon;

    /**
     * @param Carbon $end_at
     */
    public function setEndAt(Carbon $end_at): void;

}
