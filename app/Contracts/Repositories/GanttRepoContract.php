<?php

namespace App\Contracts\Repositories;

use App\Contracts\Entities\Gantt;
use Illuminate\Database\Eloquent\Collection;

interface GanttRepoContract extends BaseRepoContract
{
    /**
     * @return Gantt
     */
    public function make(): Gantt;

    /**
     * @param $id
     *
     * @return Gantt|null
     */
    public function findById($id): ?Gantt;
}
