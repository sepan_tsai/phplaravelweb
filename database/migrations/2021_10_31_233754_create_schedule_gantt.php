<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleGantt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_gantt', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('event');
            $table->date('start_at');
            $table->date('end_at');
            $table->timestamps();

            $table->index(['id']);
            $table->index(['start_at']);
            $table->index(['end_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_gantt');
    }
}
